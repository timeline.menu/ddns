<?php
/*
 * author               timeline.menu@gmail.com
 * copyright            timeline.menu has all rights reserved.
 * changelog            
 *                      +0800 2024-04-10 16:20:29 400550629 101 15 3
 *                       Create this file.
 *                        
 */

?>
<?php



{ // -- 找出token下的所有domain --

  $uri = $dns["api"]["u"] ;
  //var_dump ( $uri ) ;

  $http_header_1 
    =
    [
    //$dns["api"]["header"]["accept"],
    $dns["api"]["header"]["Authorization"],
    $dns["api"]["header"]["Content-Type"],
    ]
      ;
    //var_dump ( $http_header_1 ) ;

    // 需要是一个数组，像这样是会出错的：
    //$http_header_1 = $dns["api"]["header"]["Authorization"] ;

    $dns["domain"]["all"] = g_a_d ( $uri, $http_header_1 ) ;
    //var_dump ( $dns["domain"]["all"] ) ;

} // -- 找出token下的所有domain ^ --



{ // -- 在所有domain中查找  $dns["domain_c"] --

  //var_dump ( count ( $dns["domain"]["all"] ) ) ;
  //var_dump ( count ( $dns["domain"]["all"]["data"] ) ) ;
  //var_dump ( $dns["domain"]["all"]["data"] ) ;
  //var_dump ( $dns["domain"]["all"]["results"] ) ;
  //var_dump ( $dns["domain"]["all"]["pages"] ) ;
  //var_dump ( $dns["domain"]["all"]["page"] ) ;

  // 清空变量
  $dns["api"]["domain_id"] = "" ;

  // 循环处理 data 中的每一项

  $i = 0 ;
  $end = 100 ;

  while ( $i < $dns["domain"]["all"]["results"] )
  {

    //var_dump ( $dns["domain"]["all"]["data"]["{$i}"] ) ;
    //var_dump ( $dns["domain"]["all"]["data"]["{$i}"]["domain"] ) ;
    //var_dump ( $dns["domain"]["all"]["data"]["{$i}"]["id"] ) ;
    //var_dump ( $dns["domain"]["all"]["data"]["{$i}"]["status"] ) ;
    //var_dump ( $dns["domain"]["all"]["data"]["{$i}"]["created"] ) ;
    //var_dump ( $dns["domain"]["all"]["data"]["{$i}"]["updated"] ) ;
    //var_dump ( "----" ) ;

    if ( $dns["domain"]["all"]["data"]["{$i}"]["domain"] == $dns["domain_c"] )
    {
      // 记录这一条的id
      //var_dump ( $dns["domain"]["all"]["data"]["{$i}"]["id"] ) ;
      $dns["api"]["domain_id"] = $dns["domain"]["all"]["data"]["{$i}"]["id"] ;
    }
    else
    {
      // 什么都不做
    }


    $i ++ ;

    // 退出保险
    if ( $i > $end )
    {
      break 1 ;
    }

  }
  ;

} // -- 在所有domain中查找  $dns["domain_c"] ^ --




// 更新时间戳
include "timestamp.php" ;

$txt = "--" . "\n" . $dns["timestamp"] . "\n" . "domain_id: " . $dns["api"]["domain_id"] . "\n" . "" ;
fwrite ( $dns["fo"], $txt ) ;



//var_dump ( $dns["api"]["domain_id"] ) ;

if ( $dns["api"]["domain_id"] === "" )
{
  // 没找到对应的domain
  var_dump ( "没找到对应的domain，程序退出" ) ;

  // 更新时间戳
  include "timestamp.php" ;

  $txt = "--" . "\n" . $dns["timestamp"] . "\n" . "没找到对应的domain，程序退出" . "\n" . "" ;
  fwrite ( $dns["fo"], $txt ) ;

  exit ( -1 ) ;
}







?>
