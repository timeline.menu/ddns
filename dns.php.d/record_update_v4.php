<?php
/*
 * author               timeline.menu@gmail.com
 * copyright            timeline.menu has all rights reserved.
 * changelog            
 *                      +0800 2024-04-10 18:31:15 215437434 101 15 3
 *                       Create this file.
 *                        
 */

?>
<?php


{ // -- 修改一条解析记录 --

// linode 的解析修改命令会接受与原有解析值一样的值
// 即使要升级到与原来解析值一样的值，api也会接受并执行成功
// 这不同于阿里

  $uri_a_u = $uri_a_r . $dns["api"]["record_v4_id"] ;
  $uri = $uri_a_u ;
  //var_dump ( $uri ) ;

  $http_header_1 
    =
    [
    //$dns["api"]["header"]["accept"],
    $dns["api"]["header"]["Authorization"],
    $dns["api"]["header"]["Content-Type"],
    ]
      ;
    //var_dump ( $http_header_1 ) ;


    unset ( $outdata_1 ) ;
    $outdata_1 
      =
      '
      {
        "name": "' . $dns["domain_2"] . '",
        "target": "' . $dns["ipv4"] . '",
        "priority": 50,
        "weight": 50,
        "port": 80,
        "service": null,
        "protocol": null,
        "ttl_sec": 604800,
        "tag": null
      }
    ' 
      ;
    //var_dump ( $outdata_1 ) ;


    $dns["record"]["v4"]["update"] = r_u ( $uri, $http_header_1, $outdata_1 ) ;
    //var_dump ( $dns["record"]["v4"]["update"] ) ;

} // -- 修改一条解析记录 ^ --




if ( array_key_exists ( 'id', $dns["record"]["v4"]["update"] ) ) 
{
  var_dump ( "record update success. the id is :" ) ;
  var_dump ( $dns["record"]["v4"]["update"]["id"] ) ;

  // 更新时间戳
  include "timestamp.php" ;

  $txt = "--" . "\n" . $dns["timestamp"] . "\n" . "record update success. the id is :" . $dns["record"]["v4"]["update"]["id"] . "\n" . "" ;
  fwrite ( $dns["fo"], $txt ) ;

}

if ( array_key_exists ( 'errors', $dns["record"]["v4"]["update"] ) ) 
{
  var_dump ( "record update fail !!! the error is :" ) ;
  var_dump ( $dns["record"]["v4"]["update"]["errors"] ) ;

  // 更新时间戳
  include "timestamp.php" ;

  $txt = "--" . "\n" . $dns["timestamp"] . "\n" . "record update fail !!! the error is :" . $dns["record"]["v4"]["update"]["errors"] . "\n" . "" ;
  fwrite ( $dns["fo"], $txt ) ;

}







?>
