<?php
/*
 * author               timeline.menu@gmail.com
 * copyright            timeline.menu has all rights reserved.
 * changelog            
 *                      +0800 2024-04-09 20:22:51 468444897 100 15 2
 *                       Create this file.
 *                        
 */

?>
<?php

{ // -- 定义各种需要用到的路径 --

  // 这个是log的基路径
  // 由于具体执行php的用户不一定具有这个路径的权限，所以需要部署前手动建立并赋给这个路径读写权限
  $dns["log"]["dir"]["base"] = "/mnt/logs/ddns/" ;

  // 这个是具体的日志文件名称的前导字符
  $dns["log"]["file"]["prefix"] = "ddns_linode_" ;

} // -- 定义各种需要用到的路径 ^ --


// 检查日志基路径是否存在
//var_dump ( is_dir ( $dns["log"]["dir"]["base"] ) ) ;
$dns["log_dir_base_exist"] = is_dir ( $dns["log"]["dir"]["base"] ) ;
if ( $dns["log_dir_base_exist"] === true )
{
  //exit ( -1 ) ;
}
else
{
  exit ( -1 ) ;
}

//var_dump ( "test1" ) ;





// 得到日期中的年份
include "time_year.php" ;
//var_dump ( $dns["time"]["year"] ) ;

// 得到日期中的月份
include "time_month.php" ;
//var_dump ( $dns["time"]["month"] ) ;





// 组合日志路径
$dns["log"]["dir"]["month"] = $dns["log"]["dir"]["base"] . "/" . $dns["time"]["year"] . "/" . $dns["time"]["month"] . "/" ;
//var_dump ( $dns["log"]["dir"]["month"] ) ;

// 检查日志路径是否存在
//var_dump ( is_dir ( $dns["log"]["dir"]["month"] ) ) ;
$log_dir_base_exist = is_dir ( $dns["log"]["dir"]["month"] ) ;
if ( $log_dir_base_exist === true )
{
  //exit ( -1 ) ;
  // 月路径存在时
}
else
{
  //exit ( -1 ) ;
  // 月路径不存在时
  // 建立这个路径

  //if ( 0 == 1 )
  { // -- 建立这个路径 --
    unset ( $cmd_1 ) ;
    $cmd_1 = "mkdir -p " . $dns["log"]["dir"]["month"] ;
    //var_dump ( $cmd_1 ) ;
    unset ( $exec_1 ) ;
    unset ( $output_1 ) ;
    unset ( $status_1 ) ;
    $exec_1 = exec ( $cmd_1, $output_1, $status_1 ) ;
    //var_dump ( $exec_1 ) ;
    //var_dump ( $output_1 ) ;
    //var_dump ( $status_1 ) ;

    if ( $status_1 == "0" )
    { // -- 执行成功完成 --
      //echo "执行成功完成\n" ;  
    } // -- 执行成功完成 ^ --
    else if ( $status_1 == "1" )
    { // -- 执行失败 --
      //echo "执行失败\n" ;
      exit ( -1 ) ;
    } // -- 执行失败 ^ --
    else
    { // -- 执行的其他返回情况 --
      //echo "执行的其他返回情况\n" ;
      exit ( -1 ) ;
    } // -- 执行的其他返回情况 ^ --
  } // -- 建立这个路径 ^ --

}

//var_dump ( "test2" ) ;






// 得到日期中的日号
include "time_day.php" ;
//var_dump ( $dns["time"]["day"] ) ;



// 组合出完整的日志文件
$dns["log"]["file"]["complete"] = $dns["log"]["dir"]["month"] . $dns["log"]["file"]["prefix"] . $dns["time"]["year"] . "_" . $dns["time"]["month"] . "_" . $dns["time"]["day"] . ".txt" ;
var_dump ( $dns["log"]["file"]["complete"] ) ;

// 打开这个文件以备写入
$dns["fo"] = fopen ( $dns["log"]["file"]["complete"] , "a+" ) ;
//var_dump ( $dns["fo"] ) ;

if ( $dns["fo"] == false )
{ // -- 文件打开失败 --
  var_dump ( "文件打开失败" ) ;
  exit ( -1 ) ;
} // -- 文件打开失败 ^ --
else
{ // -- 文件打开成功 --
  var_dump ( "文件打开成功" ) ;
} // -- 文件打开成功 ^ --






// 先写入一个前导分隔符
//$txt = "---- ---- ---- ----" . "\n" ;

$txt = "\n" ;
fwrite ( $dns["fo"], $txt ) ;

$txt = "---- ---- ---- ----";
fwrite ( $dns["fo"], $txt ) ;

$txt = "\n" ;
fwrite ( $dns["fo"], $txt ) ;


//if ( 0 == 1 )
{ // -- 记录ipv4地址 --

  // 更新时间戳
  include "timestamp.php" ;

  $txt = "--" . "\n" . $dns["timestamp"] . "\n" . $dns["ipv4"] . "\n" . "" ;
  fwrite ( $dns["fo"], $txt ) ;

} // -- 记录ipv4地址 ^ --

//if ( 0 == 1 )
{ // -- 记录ipv6地址 --

  // 更新时间戳
  include "timestamp.php" ;

  $txt = "--" . "\n" . $dns["timestamp"] . "\n" . $dns["ipv6"] . "\n" . "" ;
  fwrite ( $dns["fo"], $txt ) ;

} // -- 记录ipv6地址 ^ --







//var_dump ( fclose ( $dns["fo"] ) ) ;
//fclose ( $dns["fo"] ) ;
// 关闭文件流的功能移动到了 log.close.php 中了。
// 因为在这个文件之后在本程序中还有其它要写入日志的需求



?>
