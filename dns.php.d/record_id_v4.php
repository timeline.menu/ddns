<?php
/*
 * author               timeline.menu@gmail.com
 * copyright            timeline.menu has all rights reserved.
 * changelog            
 *                      +0800 2024-04-10 18:11:07 666280027 101 15 3
 *                       Create this file.
 *                        
 */




//var_dump ( $dns["api"]["record_v4_id"] ) ;

if ( $dns["api"]["record_v4_id"] === "" )
{
  // 没找到对应的ipv4 record
  var_dump ( "没找到对应的ipv4 record" ) ;
  //exit ( -1 ) ;

  // 更新时间戳
  include "timestamp.php" ;

  $txt = "--" . "\n" . $dns["timestamp"] . "\n" . "没找到对应的ipv4 record" . "\n" . "" ;
  fwrite ( $dns["fo"], $txt ) ;

  // 新建
  include "record_create_v4.php" ;

}
else
{
  // 找到了对应的ipv4 record
  var_dump ( "找到了对应的ipv4 record" ) ;

  // 更新时间戳
  include "timestamp.php" ;

  $txt = "--" . "\n" . $dns["timestamp"] . "\n" . "找到了对应的ipv4 record" . "\n" . "" ;
  fwrite ( $dns["fo"], $txt ) ;

  // 检查record里的target是否与现在的ip地址对应
  //var_dump ( $dns["api"]["record_v4_target"] ) ;
  //var_dump ( $dns["ipv4"] ) ;

  //include "record_create_v4.php" ; // 测试用

  //var_dump ( $dns["api"]["record_v4_target"] == $dns["ipv4"] ) ;

  if ( $dns["api"]["record_v4_target"] == $dns["ipv4"] )
  {
    // 解析记录的值与现在的ipv4地址是对应的
    var_dump ( "解析记录的值与现在的ipv4地址是对应的" ) ;

    // 更新时间戳
    include "timestamp.php" ;

    $txt = "--" . "\n" . $dns["timestamp"] . "\n" . "解析记录的值与现在的ipv4地址是对应的" . "\n" . "" ;
    fwrite ( $dns["fo"], $txt ) ;

    //include "record_update_v4.php" ; // 测试用
  }
  else
  {
    // 解析记录的值与现在的ipv4地址是不对应的
    var_dump ( "解析记录的值与现在的ipv4地址是不对应的" ) ;

    // 更新时间戳
    include "timestamp.php" ;

    $txt = "--" . "\n" . $dns["timestamp"] . "\n" . "解析记录的值与现在的ipv4地址是不对应的" . "\n" . "" ;
    fwrite ( $dns["fo"], $txt ) ;

    // 更新记录值
    include "record_update_v4.php" ;

  }

}

?>
