<?php
/*
 * author               timeline.menu@gmail.com
 * copyright            timeline.menu has all rights reserved.
 * changelog            
 *                      +0800 2024-04-10 17:30:15 582989844 101 15 3
 *                       Create this file.
 *                        
*/

?>
<?php



// create a recode
function r_c ( $uri, $http_header, $data ) 
{ // --  --
  { // -- 全局变量 --
    global $dns ;
  } // -- 全局变量 ^ --
  { // -- 变量预定义，只包括本文件中定义的变量，并不包括被引入的文件中定义的变量 --
    $retarr = [] ; // -- 结果放在这里面，函数完成后将其返回给主调 --
    $retval = "" ; // -- 返回值 --
  } // -- 变量预定义，只包括本文件中定义的变量，并不包括被引入的文件中定义的变量 ^ --
  { // -- 逻辑处理 --
    { // -- 对变量进行处理 --
    } // -- 对变量进行处理 ^ --
    { // -- 其它逻辑过程 --



  //var_dump ( "---- function r_c ----" ) ;

  //var_dump ( $uri ) ;
  //var_dump ( $http_header ) ;
  //var_dump ( $data ) ;
 




      { // -- curl 获取数据 --
        $url = $uri ;
        //var_dump ( $url ) ;

        $curl = curl_init() ;

        curl_setopt ( $curl, CURLOPT_URL, $url ) ;
        curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true ) ;
        curl_setopt ( $curl, CURLOPT_HEADER, false ) ;
        curl_setopt ( $curl, CURLOPT_HTTPHEADER, $http_header ) ;
        curl_setopt ( $curl, CURLOPT_CUSTOMREQUEST, "POST" ) ;
        //curl_setopt ( $curl, CURLOPT_POST, 1 ) ;
        curl_setopt ( $curl, CURLOPT_POSTFIELDS, $data ) ;

        $data = curl_exec ( $curl ) ;
        //var_dump ( $data ) ;
        //echo $data ;

        curl_close ( $curl ) ;
      } // -- curl 获取数据 ^ --


      //var_dump ( curl_errno ( $curl ) ) ;
      if ( curl_errno ( $curl ) )
      {
        //请求失败，返回错误信息
        var_dump ( curl_errno ( $curl ) ) ;
        var_dump ( curl_error ( $curl ) ) ;
        //var_dump ( "---- function r_c ^ ----" ) ;
      }
      else
      { // -- curl 成功时 --
        //请求成功
        //var_dump ( $data ) ;
        //return $data ;

        $json_decode = json_decode ( $data, true ) ;
        //var_dump ( "---- function r_c ^ ----" ) ;
        return $json_decode ;

      } // -- curl 成功时 ^ --




    } // -- 其它逻辑过程 ^ --
  } // -- 逻辑处理 ^ --
} // --  ^ --





?>
