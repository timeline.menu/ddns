<?php
/*
 * author               timeline.menu@gmail.com
 * copyright            timeline.menu has all rights reserved.
 * changelog            
 *                      +0800 2024-04-10 17:11:22 322361647 101 15 3
 *                       Create this file.
 *                        
 */

?>
<?php




// 列出这个 domain 的所有解析记录

{ // -- 列出这个 domain 的所有解析记录 --

  $uri_a_r = $dns["api"]["u"] . $dns["api"]["domain_id"] . "/records/" ;
  $uri = $uri_a_r ;
  //var_dump ( $uri ) ;

  $http_header_1 
    =
    [
    //$dns["api"]["header"]["accept"],
    $dns["api"]["header"]["Authorization"],
    $dns["api"]["header"]["Content-Type"],
    ]
      ;
    //var_dump ( $http_header_1 ) ;


    $dns["record"]["all"] = g_a_r ( $uri, $http_header_1 ) ;
    //var_dump ( $dns["record"]["all"] ) ;

} // -- 列出这个 domain 的所有解析记录 ^ --

  // 清空变量
$dns["api"]["record_v4_id"] = "" ;
$dns["api"]["record_v4_target"] = "" ;
$dns["api"]["record_v6_id"] = "" ;
$dns["api"]["record_v6_target"] = "" ;


{ // -- 在所有domain中查找  $dns["domain_c"] --

  //var_dump ( count ( $dns["record"]["all"] ) ) ;
  //var_dump ( count ( $dns["record"]["all"]["data"] ) ) ;
  //var_dump ( $dns["record"]["all"]["data"] ) ;
  //var_dump ( $dns["record"]["all"]["results"] ) ;
  //var_dump ( $dns["record"]["all"]["pages"] ) ;
  //var_dump ( $dns["record"]["all"]["page"] ) ;


  // 循环处理 data 中的每一项

  $i = 0 ;
  $end = 100 ;

  while ( $i < $dns["record"]["all"]["results"] )
  {
    //var_dump ( "----" ) ;
    //var_dump ( $dns["record"]["all"]["data"]["{$i}"] ) ;
    //var_dump ( $dns["record"]["all"]["data"]["{$i}"]["name"] ) ;
    //var_dump ( $dns["record"]["all"]["data"]["{$i}"]["id"] ) ;
    //var_dump ( $dns["record"]["all"]["data"]["{$i}"]["type"] ) ;
    //var_dump ( $dns["record"]["all"]["data"]["{$i}"]["target"] ) ;
    //var_dump ( $dns["record"]["all"]["data"]["{$i}"][""] ) ;
    //var_dump ( $dns["record"]["all"]["data"]["{$i}"]["created"] ) ;
    //var_dump ( $dns["record"]["all"]["data"]["{$i}"]["updated"] ) ;
    //var_dump ( "----^" ) ;


    //var_dump ( $dns["domain_2"] ) ;
    //var_dump ( $dns["record"]["all"]["data"]["{$i}"]["name"] == $dns["domain_2"] && $dns["record"]["all"]["data"]["{$i}"]["type"] == "A" ) ;

    if ( $dns["record"]["all"]["data"]["{$i}"]["name"] == $dns["domain_2"] && $dns["record"]["all"]["data"]["{$i}"]["type"] == "A" )
    {
      // A 记录
      // 记录这一条的id
      //var_dump ( $dns["record"]["all"]["data"]["{$i}"]["id"] ) ;
      $dns["api"]["record_v4_id"] = $dns["record"]["all"]["data"]["{$i}"]["id"] ;
      $dns["api"]["record_v4_target"] = $dns["record"]["all"]["data"]["{$i}"]["target"] ;
    }



    //var_dump ( $dns["record"]["all"]["data"]["{$i}"]["name"] == $dns["domain_2"] && $dns["record"]["all"]["data"]["{$i}"]["type"] == "AAAA" ) ;

    if ( $dns["record"]["all"]["data"]["{$i}"]["name"] == $dns["domain_2"] && $dns["record"]["all"]["data"]["{$i}"]["type"] == "AAAA" )
    {
      // AAAA 记录
      // 记录这一条的id
      //var_dump ( $dns["record"]["all"]["data"]["{$i}"]["id"] ) ;
      $dns["api"]["record_v6_id"] = $dns["record"]["all"]["data"]["{$i}"]["id"] ;
      $dns["api"]["record_v6_target"] = $dns["record"]["all"]["data"]["{$i}"]["target"] ;
    }




    $i ++ ;

    // 退出保险
    if ( $i > $end )
    {
      break 1 ;
    }

  }
  ;

} // -- 在所有domain中查找  $dns["domain_c"] ^ --



include "record_id_v4.php" ;
include "record_id_v6.php" ;






?>
