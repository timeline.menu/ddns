<?php
/*
 * author               timeline.menu@gmail.com
 * copyright            timeline.menu has all rights reserved.
 * changelog            
 *                      +0800 2024-04-10 17:38:48 095158485 101 15 3
 *                       Create this file.
 *                        
 */

?>
<?php


{ // -- 给这个 domain 新建一条解析记录 --

  //$uri_a_r = $dns["api"]["u"] . $dns["api"]["domain_id"] . "/records/" ;
  $uri = $uri_a_r ;
  //var_dump ( $uri ) ;

  $http_header_1 
    =
    [
    //$dns["api"]["header"]["accept"],
    $dns["api"]["header"]["Authorization"],
    $dns["api"]["header"]["Content-Type"],
    ]
      ;
    //var_dump ( $http_header_1 ) ;


    unset ( $outdata_1 ) ;
    $outdata_1 
      =
      '
      {
        "type": "AAAA",
        "name": "' . $dns["domain_2"] . '",
        "target": "' . $dns["ipv6"] . '",
        "priority": 50,
        "weight": 50,
        "port": 80,
        "service": null,
        "protocol": null,
        "ttl_sec": 604800
      }
    ' 
      ;
    //var_dump ( $outdata_1 ) ;


    $dns["record"]["v6"]["create"] = r_c ( $uri, $http_header_1, $outdata_1 ) ;
    //var_dump ( $dns["record"]["v6"]["create"] ) ;

} // -- 给这个 domain 新建一条解析记录 ^ --




if ( array_key_exists ( 'id', $dns["record"]["v6"]["create"] ) ) 
{
  var_dump ( "record create success. the id is :" ) ;
  var_dump ( $dns["record"]["v6"]["create"]["id"] ) ;

  // 更新时间戳
  include "timestamp.php" ;

  $txt = "--" . "\n" . $dns["timestamp"] . "\n" . "record create success. the id is :" . $dns["record"]["v6"]["create"]["id"] . "\n" . "" ;
  fwrite ( $dns["fo"], $txt ) ;

}

if ( array_key_exists ( 'errors', $dns["record"]["v6"]["create"] ) ) 
{
  var_dump ( "record create fail !!! the error is :" ) ;
  var_dump ( $dns["record"]["v6"]["create"]["errors"] ) ;

  // 更新时间戳
  include "timestamp.php" ;

  $txt = "--" . "\n" . $dns["timestamp"] . "\n" . "record create fail !!! the error is :" . $dns["record"]["v6"]["create"]["errors"] . "\n" . "" ;
  fwrite ( $dns["fo"], $txt ) ;

}







?>
