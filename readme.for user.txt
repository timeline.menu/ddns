



前置说明：


本ddns系统基于 linux 开发，使用了 php 作为运行环境。
注解：这并不意味着需要 apache 或者 nginx IIS 之类的 web service ，也许很多phper都不知道 php 的运行并不需要 web service 。


在本文中我会假设：将本程序代码拉回本地后 放在了 /mnt/apps/ddns/ 下。
如果你与我放置的路径不同的话，请记得将本文中之后的路径都一一对应。


本程序默认的日志路径是 /mnt/logs/ddns/
因为用于运行本程序的系统用户并不一定具有这个路径的权限，所以在这里需要手动做以下两步操作：
建立这个路径：
sudo mkdir -p /mnt/logs/ddns/
sudo chmod 777 /mnt/logs/ddns/




下面开始正式部署：
一共只有三步：


第一步：打开 /mnt/apps/ddns/dns.php 按自己的实际需求修改以下五个变量
$dns["dev"]
$dns["domain_0"]
$dns["domain_1"]
$dns["domain_2"]
$dns["api"]["token"]


第二步：执行：
chmod +x /mnt/apps/ddns/ipv4.sh
确保 ipv4.sh 具备可执行权限，因为我们需要它从网卡拿出ip地址
chmod +x /mnt/apps/ddns/ipv6.sh
确保 ipv6.sh 具备可执行权限，因为我们需要它从网卡拿出ip地址



第三步：是定时任务:
先打开
/mnt/apps/ddns/ddns.cron
修改下面这三行中的 <你的本地用户名> 部份，改成你要用来执行定时任务的本地用户名，默认的是expl
MAILTO=<你的本地用户名>
HOME=/home/<你的本地用户名>/
*/1 * * * * <你的本地用户名> php /mnt/apps/ddns/dns.php

接着复制到 /etc/cron.d/ 中去
sudo cp -v /mnt/apps/ddns/ddns.cron /etc/cron.d/
留意一下复制过去后的属主，需要为 root:root
ls -laZh /etc/cron.d/ddns.cron
如果不是 root:root 的话就执行
sudo chown root:root /etc/cron.d/ddns.cron


到此，本ddns程序就部署完成了。




注意：
如果你的系统没有开启 cronie.service 的开机自启的话就执行：
sudo systemctl start cronie.service
sudo systemctl enable cronie.service
一般说来 cronie.service 都是默认开机自启的，所以这一步一般不需要。
可以使用这条命令来确定一下：
sudo systemctl status cronie.service



