#! /bin/sh
 # author               timeline.menu@gmail.com
 # copyright            timeline.menu has all rights reserved.
 # changelog            
 #                      2021-02-13 16:09:24.009899680 07 6 044 CST +08:00:00
 #                       建立本文件
 #                      2021-07-18.16:37:49.945248468.29.7.199.+08
 #                       建立本文件
 #                      2023-01-12.12:10:00.611766615.03.4.012.+0800
 #                       Create this file
#



#$1 网卡名称

ip addr show $1 | grep inet | grep -v inet6 | awk '{print $2}'


