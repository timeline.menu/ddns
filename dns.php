<?php
/*
 * author               timeline.menu@gmail.com
 * copyright            timeline.menu has all rights reserved.
 * changelog            
 *                      2023-01-12.15:30:13.623750109.03.4.012.+0800
 *                       Create this file

 *                      +0800 2024-04-09 20:08:56 310189702 100 15 2
 *                       改为linode的api接口 

 include "dns.php.d/" ;

// 主程序逻辑
// 获取当前这个 token 下的 domain 列表
// 从列表中挑出目标 domain 
// - 找出其 id 号，备用
// 列出这个 domain 的所有解析记录
// - 从中查找是否有符合 $dns["domain_2"] 的解析项
// - - 如果没有，
// - - - 新建 A 记录，指向 ipv4.php 生成的地址
// - - - 新建 AAAA 记录，指向 ipv6.php 生成的地址
// - - 如果有，
// - - - 检查记录类型

// - - - 对比 A 记录，是否与 ipv4.php 生成的地址相同
// - - - - 相同，
// - - - - - 写入log
// - - - - - 程序结束
// - - - - 不相同，
// - - - - - update 解析记录
// - - - - - 写入log
// - - - - - 程序完成
// - - - 对比 AAAA 记录，是否与 ipv6.php 生成的地址相同
// - - - - 相同，
// - - - - - 写入log
// - - - - - 程序结束
// - - - - 不相同，
// - - - - - update 解析记录
// - - - - - 写入log
// - - - - - 程序完成

 *                       
 */
?>
<?php

if ( PHP_SAPI == 'cli' )
{ // -- cli --
  echo 'is cli' ;
  echo "\n" ;

  { // -- 定义变量 --

    // ----
    // 这些变量需要在部署时按照实际的情况进行修改
    // ----

    // 从哪个网卡获取ip地址
    $dns["dev"] = "ppp0" ; // 如果你的网关拨号后生成的临时点对点网卡名称不是ppp0的话，就对应的修改。

    // 顶级域名
    $dns["domain_0"] = "把这几个字替换成你的域名的顶级域名部份，比如example.com中的com" ;

    // 一级域名
    $dns["domain_1"] = "把这几个字替换成你的域名的一级域名部份，比台example.com中的example" ;

    // 二级域名
    $dns["domain_2"] = "把这几个字替换成你想要用于ddns解析的二级域名的名字，比如你想把0ddns0.example.com用于ddns解析，那么就在这里填入0ddns0" ;
    //var_dump ( $dns["domain_2"] ) ;

    // 这是linode网站上生成的token
    $dns["api"]["token"] = "把双引号里的内容改为你的linode上的token,并保留这里的前后双引号" ;

  } // -- 定义变量 ^ --



  include "dns.php.d/ipv4.php" ;
  include "dns.php.d/ipv6.php" ;
  include "dns.php.d/timestamp.php" ;

  include "dns.php.d/log.php" ;


  // api接口用到的一些参数
  include "dns.php.d/api.variables.php" ;


  include "dns.php.d/function.getalldomians.php" ;
  include "dns.php.d/function.getallrecords.php" ;
  include "dns.php.d/function.record_create.php" ;
  include "dns.php.d/function.record_update.php" ;


  include "dns.php.d/domain_id.php" ;
  include "dns.php.d/record_id.php" ;



  include "dns.php.d/log.close.php" ;


} // -- cli ^ --
else
{ // -- not cli --
  //echo 'not cli' ;
  //echo 'This script will only work in cli mode, and it is not in cli mode now. Please contact the website administrator and tell him the current access method for help.' ;
  echo 'This script cannot work in the current mode. Please contact the webmaster and tell him the current access method for help.' ;
  echo "\n" ;
} // -- not cli ^ --

?>
