#! /bin/sh
 # author               timeline.menu@gmail.com
 # copyright            timeline.menu has all rights reserved.
 # changelog            
 #                      2021-02-14 15:46:46.868206959 07 7 045 CST +08:00:00
 #                       建立本文件
 #                      2021-07-18.16:38:13.322398752.29.7.199.+08
 #                       建立本文件
 #                      2023-01-12.12:11:19.781166116.03.4.012.+0800
 #                       Create this file
#


#$1 网卡名称

ip addr show $1 | grep inet6 | grep -v deprecated | grep -v link | grep -v fd | awk '{print $2}' | awk -F/ '{print $1}' | head -1





